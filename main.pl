/*
 * INTERFAZ GRAFICA: Esta parte del Sistema Experto es la que se encarga
 * de interactuar con el usuario final, mostar imagenes, botones,
 * textos, etc.
 */

:- use_module(library(pce)).
:- pce_image_directory('./images').
:- use_module(library(pce_style_item)).
:- dynamic si/1,no/1. % Solo permita considerar SI o NO

%!
% LOAD IMAGES RESCOURCES
%

resource(portada,image,image('portada.jpg')).
resource(img_home,image,image('home.jpg')).

%!
% SHOWS IMAGES
%

view_image(Screen,Image,X,Y):- new(Figure,figure),
    new(Bitmap,bitmap(resource(Image),@on)),
    send(Bitmap,name,1),
    send(Figure,display,Bitmap),
    send(Figure,status,1),
    send(Screen,display,Figure,point(X,Y)).

%!
% BUTTONS
%

%!
% VIEWS
%

home_view:- new(@homeView,dialog('Sistema Experto Diagnosticador',size(1000,1000))),
    new(@lblText,label(nombre,'Procede a responder las siguientes preguntas para que obtenga su diagnostico',font('times','roman',14))),
    new(@lblResp,label(nombre,'',font(comic,bolb,18))),
    new(@btnConsult,button('Iniciar consulta',message(@prolog,buttons))),
    new(@btnTreatment,button('Tratamiento?')),
    new(@btnExit,button('SALIR',and(
                                    message(@homeView,destroy),
                                    message(@homeView,free)
                                ))),
    view_image(@homeView,img_home,0,0),

    send(@homeView,display,@btnConsult,point(138,450)),
    send(@homeView,display,@lblText,point(20,130)),
    send(@homeView,display,@lblResp,point(20,160)),
    send(@homeView,display,@btnExit,point(300,450)),
    send(@homeView,open_centered).

init_view:- new(@init,dialog('Bienvenido al Sistema Experto Diagnosticador',size(1000,1000))),
    view_image(@init,portada,100,80),
    new(BtnInit,button('INICIAR',and(
                                      message(@prolog,home_view),
                                      and(message(@init,destroy),message(@init,free))
                                  ))),
    new(BtnExit,button('SALIR',and(
                                    message(@init,destroy),
                                    message(@init,free)
                                ))),

    send(@init,append(BtnInit)),
    send(@init,append(BtnExit)),
    send(@init,open_centered).

ask(Problem):- new(D,dialog('Valoración inicial')),
    new(Lbl1,label(texto,'Responder a las siguientes preguntas:')),
    new(Lbl2,label(prob,Problem)),
    new(BtnYes,button(si,and(message(D,return,si)))),
    new(BtnNo,button(no,and(message(D,return,no)))),
    send(D,append,Lbl1),
    send(D,append,Lbl2),
    send(D,append,BtnYes),
    send(D,append,BtnNo),
    send(D,default_button,si),
    send(D,open_centered),
    get(D,confirm,Answer),
    write(Answer),
    send(D,destroy),
    ((Answer==si)->assert(si(Problem));assert(no(Problem)),fail).

%!
% Bugs
%

bugs('Falla en la tarjeta gráfica.'):- gpu,!.
bugs('Falla en la memoria RAM.'):- ram,!.
bugs('VIRUS.'):- virus,!.
bugs('Falla en la tarjeta madre.'):- mainboard,!.
bugs('Falla con la fuente de alimentación(batería o cable).'):- power_suply,!.
bugs('Una falla desconocida.').

%!
% RULES
%

gpu:- fail_gpu,
    question('¿La computadora da pitidos?'),
    question('¿La imagen se ve borrosa?'),
    question('¿El monitor no da imagen?').

ram:- fail_ram,
    question('¿No se abren archivos diciendo que estos están corruptos?'),
    question('¿Muestra la pantalla azul?'),
    question('¿La computadora se va haciendo más lenta según la va utilizando?'),
    question('¿La computadora se reinicia?').

virus:- have_virus,
    question('¿Falta información?'),
    question('¿La computadora se reinicia de manera inesperada?'),
    question('¿Se muestran ventanas emergentes constantemente?'),
    question('¿Tiene problemas al guardar archivos?'),
    question('¿Lentitud al usar la computadora?').

mainboard:- fail_mainboard,
    question('¿La computadora da pitidos?'),
    question('¿Muestra la pantalla azul?').

power_suply:- fail_power_supply,
    question('¿La computadora se apaga?'),
    question('¿La computadora no enciende?'),
    question('¿Tiene dificultades para encender?').

%unknow:- unkwon_failure.

%!
% Questions
%

fail_gpu:- question('¿Tiene problemas con la imagen del monitor?'),!.
fail_ram:- question('¿La computadora da pitidos?'),!.
have_virus:- question('¿No puede abrir archivos o programas?'),!.
fail_mainboard:- question('¿La computadora no enciende?'),!.
fail_power_supply:- question('¿La batería no carga y se apaga la computadora?'),!.

question(S):-(si(S)->true;(no(S)->fail;ask(S))).

%!
% Clean vars
%

clean:- retract(si(_)),fail.
clean:- retract(no(_)),fail.
clean.

%!
% Buttons
%

buttons:- clc,
    send(@btnConsult,free),
    send(@btnTreatment,free),
    bugs(Bug),
    send(@lblText,selection('Según los síntomas, es muy probable que su computadora tenga:')),
    send(@lblResp,selection(Bug)),
    send(@lblResp,font,font(comic,bolb,18)),
    new(@btnBack,button('Iniciar su evaluación',message(@prolog,buttons))),
    send(@homeView,display,@btnConsult,point(40,50)),
    send(@homeView,display,@btnTreatment,point(20,50)),
    clean.

clc:- send(@lblResp,selection('')).

:-init_view.
